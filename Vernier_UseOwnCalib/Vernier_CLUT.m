%CLUT processing for VernierX
%parameters are taken from UseLUT
%you need to specify the correct calibration files attached to the correct screens
%'Lum' are luminance readings straight out of the photometer, given in a column
%'sRGB' are the normalised RGBs that correspond to each 'Lum' reading
%sRGB runs from 0-1; (:,1)R, (:,2)G, (:,3)B
%please remember to pput the master screen on the left and slave on the right
%the master screen is ALWAYS the second last specification in UseLUT
%slave screen is always the last specification.
%why? you could have a hundred screens and calibration files for
%them all, but only the ones that you draw to (ie seond last and last) will
%be used

%if the specified screens are out of range, the program quits
%and asks that you correct the screen indices in UseLUT

%if you do not have either the Lum or sRGB files or their equivalent,
%the program overrides the CLUT and displays RGB without calibration.

%to save all that trouble, just be good and give what it needs.

%% -------- START PROGRAM -------------------------------------------------

%if you're using LUTS and each monitor has its own LUT file,
% if strcmpi(UseLUT{1}, 'yes')
if size(UseLUT, 1)>0
    
    %initialize a matrix to store clut values
    %(:,1) luminance values
    %(:,2:3) x, y
    LumCalib={}; RGBCalib={};
    
    for nLUT=1:size(UseLUT, 1)
        
        %get details of whats in each CLUT file
        a=[]; a=whos(matfile(UseLUT{nLUT, 2}));
        
        %check if the 'Lum' matrix exist
        %this matrix contains recorded luminance info
        iLum=false;
        for nLCounter=1:size(a, 1)
            iLum=strcmpi(a(nLCounter).name,'Lum');
            
            %if you found the 'Lum' matrix
            if iLum==true
                %load the lum matrix
                b = load(UseLUT{nLUT,2}, a(nLCounter).name);
                %transfer contents to the LumCalib matrix
                LumCalib{nLUT}= b.(a(nLCounter).name);
                %exit the loop
                break
            end
        end
        
        %check if the 'RGB' matrix exist
        %this matrix contains the corresponding RGB info that generated the
        %lum matrix
        iRGB=false;
        for nRGBCounter=1:size(a, 1)
            iRGB=strcmpi(a(nRGBCounter).name, 'sRGB');
            if iRGB==true
                %load the RGB matrix
                c = load(UseLUT{nLUT,2}, a(nRGBCounter).name);
                %transfer contents to the LumCalib matrix
                RGBCalib{nLUT}= c.(a(nRGBCounter).name);
                %exit the loop
                break
            end
        end
        
        %if you still cant find the lum matrix after cycling thru everything
        if iLum==false
            disp('No matching Lum matrix. Select from these available:')
            for LL=1:size(a, 1)
                %print out all the matrices names in command window
                disp([num2str(LL), ': ', a(LL).name])
            end
            %and ask to pick one
            nLCounter=[];
            nLCounter=getnum('Choose luminance matrix (0: proceed without CLUT)',0:size(a, 1));
            %load the lum matrix
            if nLCounter>0
                b = load(UseLUT{nLUT,2}, a(nLCounter).name);
                
                %transfer contents to the LumCalib cell
                LumCalib{nLUT}= b.(a(nLCounter).name);
            end
        end
        
        %if you still cant find the rgb matrix after cycling thru everything
        if iRGB==false
            disp('No matching sRGB matrix. Select from these available:')
            for rrggbb=1:size(a, 1)
                %print out all the matrices names in command window
                disp([num2str(rrggbb), ': ', a(rrggbb).name])
            end
            %and ask to pick one
            nRGBCounter=[];
            nRGBCounter=getnum(['Choose RGB matrix (0: proceed without CLUT)'],0:size(a, 1));
            
            %load the lum matrix
            if nRGBCounter>0
                c = load(UseLUT{nLUT,2}, a(nRGBCounter).name);
                
                %transfer contents to the LumCalib cell
                RGBCalib{nLUT}= c.(a(nRGBCounter).name);
            end
            
        end
    end
end

% Define black and white (white will be 1 and black 0). This is because
% in general luminace values are defined between 0 and 1 with 255 steps in
% between. All values in Psychtoolbox are defined between 0 and 1
W = WhiteIndex(screenNumber);
B = BlackIndex(screenNumber);

%if you're using CLUTS
if size(UseLUT, 1)>0 && numel(LumCalib)>0
    %index to master screen calibration file
    liAC=[1:size(UseLUT, 1)];
    iCM=liAC(cell2mat(UseLUT(:,1))==screenNumber);
    
    %quit and display error message if a matching screen cant be found
    if isempty(iCM)
        %quit and give an error asking the user to correct  UseLUT
        disp(['screens available are: ', num2str(screens)])
        disp(['correct UseLUT and re-run'])
        sca
        return
    end
    
    
    
    if numel(LumCalib{iCM})>0 && numel(RGBCalib{iCM})>0
        %difference between calibrated normalized luminance and what we want
        MCa=LumCalib{iCM};
        nMLum=MCa(:,1)-min(MCa(:,1));
        nMLum=nMLum./max(nMLum);
        
        %index of the most similar lum value to the BkgLum
        adBkgM=abs(nMLum-BkgLum);
        [iMBkg, ~]=find(adBkgM==min(adBkgM));
        % background for master screen
        cRGB=RGBCalib{iCM};
        cRGB=unique(cRGB, 'rows');
        MBkg=cRGB(iMBkg(1),:);
        
        
        %index of the most similar lum value to the TopLum
        adTopM=abs(nMLum-TopLum);
        [iTop, ~]=find(adTopM==min(adTopM));
        %top line stimulus level for master screen
        TopMStim=cRGB(iTop(1),:);
        
        %index of the most similar lum value to the TopLum
        adBotM=abs(nMLum-BotLum);
        [iBot, ~]=find(adBotM==min(adBotM));
        %bottom line stimulus level for master screen
        BotMStim=cRGB(iBot(1),:);
        
        %grey for master screen
        adGM=abs(nMLum-0.5);
        [iGray, ~]=find(adGM==min(adGM));
        %bottom line stimulus level for master screen
        MGrey=cRGB(iGray(1), :);
        
        %white
        adWM=abs(nMLum-W);
        [iW, ~]=find(adWM==min(adWM));
        white= cRGB(iW(1), :);
        
        %black
        adBM=abs(nMLum-B);
        [iB, ~]=find(adBM==min(adBM));
        black= cRGB(iB(1), :);
        
    else
        MBkg = repmat(BkgLum, [1 3]);
        TopMStim=repmat(TopLum, [1 3]);
        BotMStim=repmat(BotLum, [1 3]);
        MGrey=[0.5, 0.5, 0.5];
        white=repmat(W, [1 3]);
        black=repmat(B, [1 3]);
        
        
        
    end
    
    
    
    
    %else if not using cluts
else
    MBkg = repmat(BkgLum, [1 3]);
    TopMStim=repmat(TopLum, [1 3]);
    BotMStim=repmat(BotLum, [1 3]);
    MGrey=[0.5, 0.5, 0.5];
    white=repmat(W, [1 3]);
    black=repmat(B, [1 3]);
    
end


%if you are in dual monitor mode and everything else matches
if exist('slavescreen') && size(UseLUT,1)>1 && numel(LumCalib)>1
    iCS=liAC(cell2mat(UseLUT(:,1))==slavescreen);
    
    %quit and display error message if a matching screen cant be found
    if isempty(iCS)
        %quit and give an error asking the user to correct  UseLUT
        disp(['screens available are: ', num2str(screens)])
        disp(['correct UseLUT and re-run'])
        sca
        return
    end
    
    
    if numel(LumCalib{iCS})>0 && numel(RGBCalib{iCS})>0
        
        %difference between calibrated normalized luminance and what we want
        SCa=LumCalib{iCS};
        nSLum=SCa(:,1)-min(SCa(:,1));
        nSLum=nSLum./max(nSLum);
        
        %index of the most similar lum value to the BkgLum
        adSBkg=abs(nSLum-BkgLum);
        [iSBkg, ~]=find(adSBkg==min(adSBkg));
        % background for master screen
        cSRGB=RGBCalib{iCS};
                cSRGB=unique(cSRGB, 'rows');
        SBkg=cSRGB(iSBkg(1),:);
        
        
        %index of the most similar lum value to the TopLum
        adSTop=abs(nSLum-TopLum);
        [iSTop, ~]=find(adSTop==min(adSTop));
        %top line stimulus level for master screen
        TopSStim=cSRGB(iSTop(1),:);
        
        
        %index of the most similar lum value to the TopLum
        adSBot=abs(nSLum-BotLum);
        [iSBot, ~]=find(adSBot==min(adSBot));
        %bottom line stimulus level for master screen
        BotSStim=cSRGB(iSBot(1),:);
        
        %grey for master screen
        adSG=abs(nSLum-0.5);
        [iSGray, ~]=find(adSG==min(adSG));
        %bottom line stimulus level for master screen
        SGrey=cSRGB(iSGray(1), :); %B
        
        %white
        adSW=abs(nSLum-W);
        [iSW, ~]= find(adSW==min(adSW));
        whiteS= cSRGB(iSW(1), :);
        
        %black
        adSB=abs(nSLum-B);
        [iSB, ~]= find(adSB==min(adSB));
        blackS=cSRGB(iSB(1), :);
        
        
        
    else
        SBkg = repmat(BkgLum, [1 3]);
        TopSStim=repmat(TopLum, [1 3]);
        BotSStim=repmat(BotLum, [1 3]);
        SGrey=[0.5, 0.5, 0.5];
        whiteS=W;
        blackS=B;
        
        
    end
    
else
    SBkg = repmat(BkgLum, [1 3]);
    TopSStim=repmat(TopLum, [1 3]);
    BotSStim=repmat(BotLum, [1 3]);
    SGrey=[0.5, 0.5, 0.5];
    whiteS=W;
    blackS=B;
end


