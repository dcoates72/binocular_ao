%program file for vernier stereoacuity
%Presents two lines: one on top of the other
%subject judges if the top line appeared farther or nearer wrt the bottom
%has a special parameter that asks if we wanna rotate the screens by 90deg
%if in dual monitor mode, opens up seperate windows, not usign stereomode
%starts counting response time from the onset of the trial
%v.4 allows for screen flipping, for the current AO
%% ---- ADMIN ISSUES ------------------------------------------------------
%cd to the save directory
cd (SavePath)

%observer initials and other annoying admin
observer=input('\nPlease enter your initials\n','s');
fileType='.xls';
qt=clock;
filename=sprintf('%s%02d.%02d.%02d%s%s',observer,qt(3),qt(4),qt(5));
filename=deblank(filename);
filename=sprintf('%s%s',filename,fileType);


% some default settings for setting up Psychtoolbox
PsychDefaultSetup(2);

if strcmpi(ScrnTst, 'no')
    Screen('Preference', 'SkipSyncTests', 2);
end

% Setup Psychtoolbox for OpenGL 3D rendering support and initialize the
% mogl OpenGL for Matlab wrapper
InitializeMatlabOpenGL;

% Initialize Sounddriver
InitializePsychSound(1);

% Get the screen numbers. This gives us a number for each of the screens
% attached to our computer.
screens = Screen('Screens');

%%%%%%%%% if using cluts %%%%%%%%%%%%%%%%%%%%
% if you are not in dual monitor stereomode but you have more than 1 display
if (stereomode~=10 && numel(UseLUT)>0)
    % So in a situation where we have two screens attached we will draw to the external screen.
    screenNumber = screens(screens==UseLUT{end,1});
    %keep stereomode
    UseStereoMd=stereomode;
    
    %if the screens specified in the UseLUT dont correspond to the ones PTB found
    if isempty(screenNumber)
        %quit and give an error asking the user to correct UseLUT
        disp(['screens available are: ', num2str(screens)])
        disp(['correct UseLUT and re-run'])
        sca
        return
    end

    
    %otherwise if you are in dual monitor stereomode and enough screens
    % and clut files are specified
elseif (stereomode==10 && numel(screens)>1 && size(UseLUT,1)>1)
    %set the master screen to the second last
    %(this monitor needs to be on the left)
    screenNumber=screens(screens==UseLUT{end-1,1});
    %set the slave (ie, the right window) to be the last
    slavescreen=screens(screens==UseLUT{end,1});
    %set stereomode to monocular and use both screens as seperate displays
    UseStereoMd=0;

       %if the screens specified in the UseLUT dont correspond to the ones PTB found
    if isempty(screenNumber) || isempty(slavescreen)
        %quit and give an error asking the user to correct UseLUT
        disp(['screens available are: ', num2str(screens)])
        disp(['correct UseLUT and re-run'])
        sca
        return
    end

    
    %if you specified dual screen but not enough screens detected
elseif (stereomode==10 && numel(screens)==1 && numel(UseLUT)>0)
    NEWin= ['not enough screens for dual display, splitting single screen'];
    disp(NEWin)
    %change stereomode to split screen
    UseStereoMd=4;
    
    screenNumber = screens(screens==UseLUT{end, 1});
    
       %if the screens specified in the UseLUT dont correspond to the ones PTB found
    if isempty(screenNumber) || isempty(slavescreen)
        %quit and give an error asking the user to correct UseLUT
        disp(['screens available are: ', num2str(screens)])
        disp(['correct UseLUT and re-run'])
        sca
        return
    end

    
    %%%%%% if not using cluts %%%%%%%%%%%%%%%
    %otherwise if you are in dual monitor stereomode and enough screens
    % and clut files are specified
elseif (stereomode==10 && numel(screens)>1 && size(UseLUT,1)==0)
    %set the master screen to the second last
    %(this monitor needs to be on the left)
    screenNumber=screens(end-1);
    %set the slave (ie, the right window) to be the last
    slavescreen=screens(end);
    %set stereomode to monoc and open two seperate displays
    UseStereoMd=0;
    
    
    %if you specified dual screen but not enough screens detected
elseif (stereomode==10 && numel(screens)==1 && numel(UseLUT)==0)
    NEWin= ['not enough screens for dual display, splitting single screen'];
    disp(NEWin)
    %change stereomode to split screen
    UseStereoMd=4;
    
    screenNumber = max(screens);
    
else
    % everything else
    screenNumber = max(screens);
    %keep stereomode
    UseStereoMd=stereomode;
end


%% ---- OF CLUTs  ---------------------------------------------------------
Vernier_CLUT;

%% ---- SET UP PTB --------------------------------------------------------

% Setup Psychtoolbox for OpenGL 3D rendering support and initialize the
% mogl OpenGL for Matlab wrapper
InitializeMatlabOpenGL;

% Open a window in the attached display using PsychImaging and color it gray
%this is about the master screen, the slavescreen will reference its
%properties from here
[window, windowRect] = PsychImaging('OpenWindow', screenNumber, MGrey, [], [], [], ...
    UseStereoMd);

% % Get the centre coordinate (fixation point) of the master window in pixels.
% xCenter = screenXpixels / 2
% yCenter = screenYpixels / 2
[xCenter, yCenter] = RectCenter(windowRect);

%center the background rectangle on the middle of the screen
centeredRect = CenterRectOnPointd(windowRect, xCenter, yCenter);

% Enable alpha blending for anti-aliasing
% For help see: Screen BlendFunction?
% Also see: Chapter 6 of the OpenGL programming guide
Screen('BlendFunction', window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


% Measure the vertical refresh rate of the monitor
ifi = Screen('GetFlipInterval', window);


% Retreive the maximum priority number and set max priority
topPriorityLevel = MaxPriority(window);
Priority(topPriorityLevel);


if stereomode==10 && exist('slavescreen')
    clear b a
    
    [winS, windowRectS] = PsychImaging('OpenWindow', slavescreen, SGrey, [], [], [], UseStereoMd);
    %     PsychImaging('OpenWindow', slavescreen, SGrey, [], [], [], stereomode);
    
    % % Get the centre coordinate (fixation point) of the slave window in pixels.
    % xCenter = screenXpixels / 2
    % yCenter = screenYpixels / 2
    [xCenterS, yCenterS] = RectCenter(windowRectS);
    
    %center the background rectangle on the middle of the screen
    centeredRectS = CenterRectOnPointd(windowRectS, xCenterS, yCenterS);
    
    % Enable alpha blending for anti-aliasing
    % For help see: Screen BlendFunction?
    % Also see: Chapter 6 of the OpenGL programming guide
    Screen('BlendFunction', winS, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    % Measure the vertical refresh rate of the monitor
    ifi(end+1) = Screen('GetFlipInterval', winS);
    
end

%set the framerate to the slower of the screens
ifi=max(ifi);

if exist('Dist') && exist('ScnWH')
    %visual angle resolution of 1 pixel
    %assumes that the pixel aspect ratio is 1:1
    PxRes=(atand((ScnWH(1)./2)./Dist).*2)./ windowRect(3);
end


%% ---- TYPE 1 RESPONSE SCREEN --------------------------------------------
%position and draw text in arbitrary positions, just to get text sizes
%[xStart yStart xEnd yEnd]
Screen('TextSize', window, TxtSz);
[~,~, LFB]=DrawFormattedText(window, ' Top Far', 'center', windowRect(4).*0.25, black);
[~,~,LSB]=DrawFormattedText(window, 'Top Near', 'center', windowRect(4).*0.5, black);
[~,~, QB]=DrawFormattedText(window, 'QUIT', 'center', windowRect(4).*0.75, black);

%sizes of the text on the screen
%number of coordinates [xStart yStart xEnd yEnd]
LFSz=[0, 0, LFB(3)-LFB(1), LFB(4)-LFB(2)];
LSSz=[0, 0, LSB(3)-LSB(1), LSB(4)-LSB(2)];
QSz=[0, 0, QB(3)-QB(1), QB(4)-QB(2)];

% Make a rectangular texture to hold our text. This has the same background
% color to that of the screen. Note also, that we increase the size of the
% text bounds slightly and round upwards to the nearest pixel. This is to
% make sure the text fits in the texture and because texture dimensions can
% only be to interger pixels.
LFBRect= repmat(permute(MGrey, [1 3 2]), [ceil(LFSz(4).* 1.25), ceil(LFSz(3).* 1.1), 1]);
LFBTexture = Screen('MakeTexture', window, LFBRect);
%put text into this texture that we just made, center alignment
Screen('TextSize', LFBTexture, TxtSz);

LSBRect = repmat(permute(MGrey, [1 3 2]), [ceil(LSSz(4).* 1.25), ceil(LSSz(3).* 1.1), 1]);
LSBTexture = Screen('MakeTexture', window, LSBRect);
Screen('TextSize', LSBTexture, TxtSz);

QRect= repmat(permute(MGrey, [1 3 2]), [ceil(QSz(4).* 1.25), ceil(QSz(3).* 1.1), 1]);
QTexture = Screen('MakeTexture', window, QRect);
Screen('TextSize', QTexture, TxtSz);

%if your displays are mirrored LR 
if strcmpi(AOFlip, 'yes')
    %flip the texts as well
    DrawFormattedText(LFBTexture, 'Top Far', 'center', 'center', black, [], 1);
    DrawFormattedText(LSBTexture, 'Top Near', 'center', 'center', black, [], 1);
    DrawFormattedText(QTexture, 'QUIT', 'center', 'center', black, [], 1);
else
    %leave em alone
    DrawFormattedText(LFBTexture, 'Top Far', 'center', 'center', black);
    DrawFormattedText(LSBTexture, 'Top Near', 'center', 'center', black);
    DrawFormattedText(QTexture, 'QUIT', 'center', 'center', black);
end


if exist('slavescreen') && stereomode==10
    % Make a rectangular texture to hold our text. This has the same background
    % color to that of the screen. Note also, that we increase the size of the
    % text bounds slightly and round upwards to the nearest pixel. This is to
    % make sure the text fits in the texture and because texture dimensions can
    % only be to interger pixels.
    LFBRS =repmat(permute(SGrey, [1 3 2]), [ceil(LFSz(4).* 1.1), ceil(LFSz(3).* 1.1), 1]);
    LFBTS = Screen('MakeTexture', window, LFBRS);
    %put text into this texture that we just made, center alignment
    Screen('TextSize', LFBTS, TxtSz);

    LSBRS = repmat(permute(SGrey, [1 3 2]), [ceil(LSSz(4).* 1.1), ceil(LSSz(3).* 1.1), 1]);
    LSBTS = Screen('MakeTexture', window, LSBRS);
    Screen('TextSize', LSBTS, TxtSz);

    QRS= repmat(permute(SGrey, [1 3 2]), [ceil(QSz(4).* 1.1), ceil(QSz(3).* 1.1), 1]);
    QTS = Screen('MakeTexture', window, QRS);
    Screen('TextSize', QTS, TxtSz);
    
    if strcmpi(AOFlip, 'yes')
        %flip texts LR if the AO is flipped
        DrawFormattedText(LFBTS, 'Top Far', 'center', 'center', blackS, [], 1);
        DrawFormattedText(LSBTS, 'Top Near', 'center', 'center', blackS, [], 1);
        DrawFormattedText(QTS, 'QUIT', 'center', 'center', blackS, [], 1);
    else
        %leave em alone
        DrawFormattedText(LFBTS, 'Top Far', 'center', 'center', blackS);
        DrawFormattedText(LSBTS, 'Top Near', 'center', 'center', blackS);
        DrawFormattedText(QTS, 'QUIT', 'center', 'center', blackS);
    end
    
end

%if you are working with the keyboard
if ~strcmpi(MseKey, 'mouse')
    % Define the keyboard keys that are listened for. We will be using the left
    % and right arrow keys as response keys for the task and the escape key as
    % a exit/reset key
    %the down arrow key is used to proceed to the next trial
    escapeKey = KbName('ESCAPE');
    leftKey = KbName('LeftArrow');
    rightKey = KbName('RightArrow');
    downKey= KbName('DownArrow');

    if strcmpi(ResponseUseImages, 'yes')
        %get the path of the arrow images
        iPath=strfind(SavePath, '/results');
        ImgP=SavePath(1:iPath);

        %load left arrow key image
        LKeyLoc = [ImgP, 'keyboard_key_left.png'];
        LArIm = imread(LKeyLoc);

        %load right arrow key image
        RKeyLoc = [ImgP, 'keyboard_key_right.png'];
        RArIm = imread(RKeyLoc);

        %load esc arrow key image
        EscLoc = [ImgP, 'esc.jpg'];
        EscIm = imread(EscLoc);

        %if the AO is flipped LR
        if strcmpi(AOFlip, 'yes')

            LArIm(:,:,1)=fliplr(LArIm(:,:,1));
            LArIm(:,:,2)=fliplr(LArIm(:,:,2));
            LArIm(:,:,3)=fliplr(LArIm(:,:,3));

            RArIm(:,:,1)=fliplr(RArIm(:,:,1));
            RArIm(:,:,2)=fliplr(RArIm(:,:,2));
            RArIm(:,:,3)=fliplr(RArIm(:,:,3));

            EscIm=fliplr(EscIm);

        end
        %leave em alone and make a reguar texture
        LT = Screen('MakeTexture', window, LArIm);
        RT = Screen('MakeTexture', window, RArIm);
        ET= Screen('MakeTexture', window, EscIm);
        
    else % no images on response screen: show blanks(gray) of that size
        blankImage=zeros(IcSz(1)) + 0.5;
        LT = Screen('MakeTexture', window, blankImage);
        RT = Screen('MakeTexture', window, blankImage);
        ET= Screen('MakeTexture', window, blankImage);
    end      
        
    %reorganise image properties for presentation
    IcSzP= [0, 0, IcSz];
end

%if you are in rotated mode
if strcmpi(AORot, 'yes')
    
    
    %if the screens are flipped
    if strcmpi(AOFlip, 'yes')
     %center position of the left words
    LTC=[xCenter, windowRect(4).*(0.25)];
    %of the right words
    RTC=[xCenter, windowRect(4).*(0.75)];
    %of quit button
    QTC=[windowRect(3).*0.75, yCenter];

    else
    %center position of the left words
    LTC=[xCenter, windowRect(4).*(0.75)];
    %of the right words
    RTC=[xCenter, windowRect(4).*(0.25)];
    %of quit button
    QTC=[windowRect(3).*0.75, yCenter];
    end
   
    
    %expected coorcinates of the cursor if it were in a response box
    %[xStart yStart xEnd yEnd]
    LCur= [LTC(1)-LFSz(4)./2, LTC(2)-LFSz(3)./2, LTC(1)+LFSz(4)./2, LTC(2)+LFSz(3)./2];%left response position
    RCur=[RTC(1)-LSSz(4)./2, RTC(2)-LSSz(3)./2, LTC(1)+LSSz(4)./2, LTC(2)+LSSz(3)./2];%right response position
    QCur=[QTC(1)-QSz(4)./2, QTC(2)-QSz(3)./2, QTC(1)+QSz(4)./2, QTC(2)+QSz(3)./2];%right response position
    
        if stereomode==10 && exist('slavescreen')
            
            
            %if AO is flipped
            if strcmpi(AOFlip, 'yes')
                %center position of the left words
                LTS=[xCenterS, windowRectS(4).*(0.25)];
                %of the right words
                RTS=[xCenterS, windowRectS(4).*(0.75)];
                %of quit button
                QTSS=[windowRectS(3).*0.75, yCenterS];
                
                %otherwise leave it unflipped
            else
                %center position of the left words
                LTS=[xCenterS, windowRectS(4).*(0.75)];
                %of the right words
                RTS=[xCenterS, windowRectS(4).*(0.25)];
                %of quit button
                QTSS=[windowRectS(3).*0.75, yCenterS];
            end
            
            
        
        %expected coorcinates of the cursor if it were in a response box
        %[xStart yStart xEnd yEnd]
        LCurS= [LTS(1)-LFSz(4)./2, LTS(2)-LFSz(3)./2, LTS(1)+LFSz(4)./2, LTS(2)+LFSz(3)./2];%left response position
        RCurS=[RTS(1)-LSSz(4)./2, RTS(2)-LSSz(3)./2, LTS(1)+LSSz(4)./2, LTS(2)+LSSz(3)./2];%right response position
        QCurS=[QTSS(1)-QSz(4)./2, QTSS(2)-QSz(3)./2, QTSS(1)+QSz(4)./2, QTSS(2)+QSz(3)./2];%right response position
    end
    
    
    
    %otheriwse in upright mode
else
    
    %if screens are flipped
    if strcmpi(AOFlip, 'yes')
        %center position of the left words
        LTC=[windowRect(3).*(2./3), yCenter];
        %of the right words
        RTC=[windowRect(3).*(1./3), yCenter];
        %of quit button
        QTC=[xCenter, windowRect(4).*0.75];
        
       %otherwise keep it regular position
    else
    %center position of the left words
    LTC=[windowRect(3)./3, yCenter];
    %of the right words
    RTC=[windowRect(3).*(2./3), yCenter];
    %of quit button
    QTC=[xCenter, windowRect(4).*0.75];
    end
    
    %expected coorcinates of the cursor if it were in a response box
    %[xStart yStart xEnd yEnd]
    LCur= [LTC(1)-LFSz(3)./2, LTC(2)-LFSz(4)./2, LTC(1)+LFSz(3)./2, LTC(2)+LFSz(4)./2];%left response position
    RCur=[RTC(1)-LSSz(3)./2, RTC(2)-LSSz(4)./2, LTC(1)+LSSz(3)./2, LTC(2)+LSSz(4)./2];%right response position
    QCur=[QTC(1)-QSz(3)./2, QTC(2)-QSz(4)./2, QTC(1)+QSz(3)./2, QTC(2)+QSz(4)./2];%right response position
    
        if stereomode==10 && exist('slavescreen')
            
            if strcmpi(AOFlip, 'yes')
                %center position of the left words
                LTS=[windowRectS(3).*(2./3), yCenterS];
                %of the right words
                RTS=[windowRectS(3).*(1./3), yCenterS];
                %of quit button
                QTSS=[xCenterS, windowRectS(4).*0.75];
            else
                %center position of the left words
                LTS=[windowRectS(3).*(1./3), yCenterS];
                %of the right words
                RTS=[windowRectS(3).*(2./3), yCenterS];
                %of quit button
                QTSS=[xCenterS, windowRectS(4).*0.75];
            end
        
        
        %expected coorcinates of the cursor if it were in a response box
        %[xStart yStart xEnd yEnd]
        LCurS= [LTS(1)-LFSz(3)./2, LTS(2)-LFSz(4)./2, LTS(1)+LFSz(3)./2, LTS(2)+LFSz(4)./2];%left response position
        RCurS=[RTS(1)-LSSz(3)./2, RTS(2)-LSSz(4)./2, LTS(1)+LSSz(3)./2, LTS(2)+LSSz(4)./2];%right response position
        QCurS=[QTSS(1)-QSz(3)./2, QTSS(2)-QSz(4)./2, QTSS(1)+QSz(3)./2, QTSS(2)+QSz(4)./2];%right response position
    end

    
end

%get image center positions if you are working with the keyboard
if ~strcmpi(MseKey, 'mouse')
    %center position of images
    LIC=LTC;
    RIC=RTC;
    QIC=QTC;
    
    %center coordinates of images
    LRect=CenterRectOnPointd(IcSzP, LIC(1), LIC(2));
    RRect=CenterRectOnPointd(IcSzP, RIC(1), RIC(2));
    ERect=CenterRectOnPointd(IcSzP, QIC(1), QIC(2)); 
    
    %if you are in rotated mode
    if strcmpi(AORot, 'yes')
        %adjust text position
        LTC(1)=LTC(1)-(IcSz(2).*1.5);
        RTC(1)=RTC(1)-(IcSz(2).*1.5);
        QTC(1)=QTC(1)-(IcSz(2).*1.5);
    else
        %adjust text position
        LTC(2)=LTC(2)-(IcSz(2).*1.5);
        RTC(2)=RTC(2)-(IcSz(2).*1.5);
        QTC(2)=QTC(2)-(IcSz(2).*1.5);
    end
    
    
end

%get center coordinates of the text textures
LTextC=CenterRectOnPointd(LFSz, LTC(1), LTC(2));
RTextC=CenterRectOnPointd(LSSz, RTC(1), RTC(2));
QTextC=CenterRectOnPointd(QSz, QTC(1), QTC(2));

if stereomode==10 && exist('slavescreen')
    
    %center position of images
    LIS=LTS;
    RIS=RTS;
    QIS=QTSS;
    
    %center coordinates of images
    LRectS=CenterRectOnPointd(IcSzP, LIS(1), LIS(2));
    RRectS=CenterRectOnPointd(IcSzP, RIS(1), RIS(2));
    ERectS=CenterRectOnPointd(IcSzP, QIS(1), QIS(2));
    
    %if you are in rotated mode
    if strcmpi(AORot, 'yes')
        %adjust text position
        LTS(1)=LTS(1)-(IcSz(2).*1.5);
        RTS(1)=RTS(1)-(IcSz(2).*1.5);
        QTSS(1)=QTSS(1)-(IcSz(2).*1.5);
    else
        %adjust text position
        LTS(2)=LTS(2)-(IcSz(2).*1.5);
        RTS(2)=RTS(2)-(IcSz(2).*1.5);
        QTSS(2)=QTSS(2)-(IcSz(2).*1.5);
    end
    
    
    
    %get center coordinates of the text textures
    LTextS=CenterRectOnPointd(LFSz, LTS(1), LTS(2));
    RTextS=CenterRectOnPointd(LSSz, RTS(1), RTS(2));
    QTextS=CenterRectOnPointd(QSz, QTSS(1), QTSS(2));
end



%% ---- SOUND -------------------------------------------------------------
% Start immediately (0 = immediately)
startCue = 0;

% Should we wait for the device to really start (1 = yes)
% INFO: See help PsychPortAudio
waitForDeviceStart = 1;

% Open Psych-Audio port, with the follow arguements
% (1) [] = default sound device
% (2) 1 = sound playback only
% (3) 1 = default level of latency
% (4) Requested frequency in samples per second
% (5) 2 = stereo putput
pahandle = PsychPortAudio('Open', [], 1, 1, Samp, nrchannels);

% Set the volume to half for this demo
PsychPortAudio('Volume', pahandle, Vol);

% Make a beep which we will play back to the user
FBeep = MakeBeep(Freq, FBL./1000, Samp);



%% ---- TRIAL STUFF -------------------------------------------------------
%parameters if screen is rotated
if strcmpi(AORot, 'yes')
    PosPx=[-StimPos(2), StimPos(1)]./PxRes;
else
    %position of the stimulus in Px
    PosPx=StimPos./PxRes;
end

%size of the top line in px
%[length, width]
TopPx=TopSz./PxRes;

%size of the bot line in px
%[length, width]
BotPx=BotSz./PxRes;

%half spacing between the lines in px
SpcPx= (LinSpc./PxRes)./2;
%... x and y components [x, y] of the half spacing (how much deviation from the center)
hxySp=[SpcPx.*cosd(Ori), SpcPx.*sind(Ori)];

%number of px in the x and y dimension for the top line [x, y]
xyTop=[TopPx(1).*cosd(Ori), TopPx(1).*sind(Ori)];
%... and the bottom line
xyBot=[BotPx(1).*cosd(Ori), BotPx(1).*sind(Ori)];


if strcmpi(AORot, 'yes')
    %position of the top line without disparity
%[x-start, x-end; y-start, y-end]
TopCoord= [PosPx(1)-hxySp(2), PosPx(1)-hxySp(1)-xyTop(2); ...
    PosPx(2)-hxySp(1), PosPx(2)-hxySp(1)-xyTop(1)];
    
%position of the bottom line without disparity
%[x-start, x-end; y-start, y-end]
BotCoord= [PosPx(1)+hxySp(2), PosPx(1)+hxySp(2)+xyBot(2); ...
    PosPx(2)+hxySp(1), PosPx(2)+hxySp(1)+xyBot(1)];
    
else
%position of the top line without disparity
%[x-start, x-end; y-start, y-end]
TopCoord= [PosPx(1)+hxySp(1), PosPx(1)+hxySp(1)+xyTop(1); ...
    PosPx(2)-hxySp(2), PosPx(2)-hxySp(2)-xyTop(2)];

%position of the bottom line without disparity
%[x-start, x-end; y-start, y-end]
BotCoord= [PosPx(1)-hxySp(1), PosPx(1)-hxySp(1)-xyBot(1); ...
    PosPx(2)+hxySp(2), PosPx(2)+hxySp(2)+xyBot(2)];
end
%make list of all possible disparity combination between the top and bottom
%lines
[tDisp, bDisp]=meshgrid(TopDisp, BotDisp);
uT=[tDisp(:), bDisp(:)];

%make blocks of unique trials
%[top disp, bottom disp]
bD=zeros(nT, size(uT, 1));
for reps=1:nT
    bD(reps, :)=randperm(size(uT, 1));
end
bD=bD';
bD=bD(:);
AllTrials=uT(bD,:);

%number of trials in total
nTrials=size(AllTrials, 1);

%number of frames that would be presented in one trial
TsnFrT=round((1./ifi).*(TsStimTime./1000));

%make practice trials
PT=uT((uT(:,1)-uT(:,2))~=0,:);
%randomly choose some of them to present
iP=randi(size(PT, 1), [nPrac 1]);
PracTrials=PT(iP,:);

%concatenate big list of all trails including practices in terms of pixel
%disparities
TrPlPr=[PracTrials; AllTrials];
TrPlPr=TrPlPr./PxRes;

%make response list
%(:,1)Top disparity
%(:,2)Bottom disparity
%(:,3)response: 1=Top Far, 2=Top Near
%(:,4)correct / wrong
%(:,5)reaction time
RespList=zeros(nTrials, 5);
RespList(:,1:2)=AllTrials;

CorResp=[];
%% ---- START PRESENTATION LOOP -------------------------------------------
for t=1:nTrials+nPrac
    
    if strcmpi(AORot, 'yes')
        
        %if the AO is flipped LR
        if strcmpi(AOFlip, 'yes')
        %position of the top line with disparity [xstart xend; ystart, yend]
        LTopPos=[]; LTopPos=[TopCoord(1,:); TopCoord(2,:)-(TrPlPr(t,1)./2)];
        RTopPos=[]; RTopPos=[TopCoord(1,:); TopCoord(2,:)+(TrPlPr(t,1)./2)];
        
        %position of the bottom line with disparity [xstart xend; ystart, yend]
        LBotPos=[]; LBotPos=[BotCoord(1,:); BotCoord(2,:)-(TrPlPr(t,2)./2)];
        RBotPos=[]; RBotPos=[BotCoord(1,:); BotCoord(2,:)+(TrPlPr(t,2)./2)];

        else
       %position of the top line with disparity [xstart xend; ystart, yend]
        LTopPos=[]; LTopPos=[TopCoord(1,:); TopCoord(2,:)+(TrPlPr(t,1)./2)];
        RTopPos=[]; RTopPos=[TopCoord(1,:); TopCoord(2,:)-(TrPlPr(t,1)./2)];
        
        %position of the bottom line with disparity [xstart xend; ystart, yend]
        LBotPos=[]; LBotPos=[BotCoord(1,:); BotCoord(2,:)+(TrPlPr(t,2)./2)];
        RBotPos=[]; RBotPos=[BotCoord(1,:); BotCoord(2,:)-(TrPlPr(t,2)./2)];
        
        end

    else
        
        
                if strcmpi(AOFlip, 'yes')
        %position of the top line with disparity [xstart xend; ystart, yend]
        LTopPos=[]; LTopPos=[TopCoord(1,:)+(TrPlPr(t,1)./2); TopCoord(2,:)];
        RTopPos=[]; RTopPos=[TopCoord(1,:)-(TrPlPr(t,1)./2); TopCoord(2,:)];
        
        %position of the bottom line with disparity [xstart xend; ystart, yend]
        LBotPos=[]; LBotPos=[BotCoord(1,:)+(TrPlPr(t,2)./2); BotCoord(2,:)];
        RBotPos=[]; RBotPos=[BotCoord(1,:)-(TrPlPr(t,2)./2); BotCoord(2,:)];
  
                else
                   
        %position of the top line with disparity [xstart xend; ystart, yend]
        LTopPos=[]; LTopPos=[TopCoord(1,:)-(TrPlPr(t,1)./2); TopCoord(2,:)];
        RTopPos=[]; RTopPos=[TopCoord(1,:)+(TrPlPr(t,1)./2); TopCoord(2,:)];
        
        %position of the bottom line with disparity [xstart xend; ystart, yend]
        LBotPos=[]; LBotPos=[BotCoord(1,:)-(TrPlPr(t,2)./2); BotCoord(2,:)];
        RBotPos=[]; RBotPos=[BotCoord(1,:)+(TrPlPr(t,2)./2); BotCoord(2,:)];
                end
    end
    
%% ---- PRESENT FIXATION ----------------------------------------------
Show_Nonius

%% ---- TEST INTERVAL -------------------------------------------------
    TS=0; Resp=[];
    while TS<=TsnFrT && isempty(Resp)
        TS=TS+1;
        
        
        % Select left-eye image buffer for drawing (buffer = 0)
   %     Screen('SelectStereoDrawBuffer', window, 0);
        
        %draw background and dots in the screen
        Screen('FillRect', window, MBkg, centeredRect);
        Screen('DrawLines', window, LTopPos, TopPx(2),  TopMStim, [xCenter yCenter], 2);
        Screen('DrawLines', window, LBotPos, BotPx(2),  BotMStim, [xCenter yCenter], 2);
        Screen('FrameRect', window, black, windowRect, 4);
        
        if strcmpi(FOnOff, 'On')==1
            Screen('DrawDots', window, [xCenter yCenter], DPx, MStim, [], 2);
        end
        
        if stereomode==10 && exist('slavescreen')
            % Select right-eye image buffer for drawing (buffer = 1)
       %     Screen('SelectStereoDrawBuffer', winS, 1);
            
            Screen('FillRect', winS, SBkg, centeredRectS);
            Screen('DrawLines', winS, RTopPos, TopPx(2),  TopSStim, [xCenterS yCenterS], 2);
            Screen('DrawLines', winS, RBotPos, BotPx(2),  BotSStim, [xCenterS yCenterS], 2);

            Screen('FrameRect', winS, blackS, windowRectS, 4);
            
            if strcmpi(FOnOff, 'On')==1
                Screen('DrawDots', winS, [xCenterS yCenterS], DPx, SStim, [], 2);
            end
            
        else
            % Select right-eye image buffer for drawing (buffer = 1)
            Screen('SelectStereoDrawBuffer', window, 1);
            
            %draw fixation dot in the screen
            Screen('FillRect', window, MBkg, centeredRect);
            Screen('DrawLines', window, RTopPos, TopPx(2),  TopMStim, [xCenter yCenter], 2);
            Screen('DrawLines', window, RBotPos, BotPx(2),  BotMStim, [xCenter yCenter], 2);
            Screen('FrameRect', window, black, windowRect, 4);
            
            if strcmpi(FOnOff, 'On')==1
                Screen('DrawDots', window, [xCenter yCenter], DPx, SStim, [], 2);
            end
            
        end
        
        
        
        % Flip to the screen. This command basically draws all of our previous
        % commands onto the screen.
        % flips the second slavescreen seperately from the first
        if TS==1
            TSvbl=[]; TSvbl=Screen('Flip', window);
            
            if stereomode==10 && exist('slavescreen')
                TSlvbl=[]; TSlvbl=Screen('Flip', winS);
            end
            
            %start timing if using keyboard
            if ~strcmpi(MseKey, 'mouse')
                %get timestamp
                onsetTime = GetSecs;
            end
            
        else
            TSvbl= Screen('Flip', window, TSvbl+ifi);
            
            if stereomode==10 && exist('slavescreen')
                TSlvbl=Screen('Flip', winS, TSlvbl+ifi);
            end
            
            
                    
  if ~strcmpi(MseKey, 'mouse')
            %check for key presses
            [keyIsDown,secs, keyCode] = KbCheck;
            
            %if the escape key was chosen
            if keyCode(escapeKey)
                Resp=0;
                offsetTime= GetSecs;
                ShowCursor;
                sca;
                break
                
                %if descending was chosen
            elseif keyCode(leftKey)
                Resp = 1;
                offsetTime= GetSecs;
                
                %if ascending was chosen
            elseif keyCode(rightKey)
                Resp = 2;
                offsetTime= GetSecs;
            end
        
  end  
        end
        
    end
    
    
    
        %% ---- RESPONSE --------------------------------------------------------
   if isempty(Resp)
        
        %if your projector is rotated 90 clockwise
    if strcmpi(AORot, 'yes')
        
        % Select left-eye image buffer for drawing (buffer = 0)
        % Screen('SelectStereoDrawBuffer', window, 0);
        
        %position and draw left side text
        Screen('FillRect', window, MGrey, centeredRect);
        Screen('DrawTexture', window, LFBTexture, [], LTextC, 270);
        Screen('DrawTexture', window, LSBTexture, [], RTextC, 270);
        Screen('DrawTexture', window, QTexture, [], QTextC, 270);
        %if there's the keyboard
        if ~strcmpi(MseKey, 'mouse')
            Screen('DrawTexture', window, LT, [], LRect, 270);
            Screen('DrawTexture', window, RT, [], RRect, 270);
            Screen('DrawTexture', window, ET, [], ERect, 270);
        end
        
        
        % Select right-eye image buffer for drawing (buffer = 1)
        % Screen('SelectStereoDrawBuffer', window, 1);
        if stereomode==10 && exist('slavescreen')
            % in dual display
            % Screen('SelectStereoDrawBuffer', winS, 1);
            
            Screen('FillRect', winS, SGrey, centeredRectS);
            Screen('DrawTexture', winS, LFBTS, [], LTextS, 270);
            Screen('DrawTexture', winS, LSBTS, [], RTextS, 270);
            Screen('DrawTexture', winS, QTS, [], QTextS, 270);
            %if there's the keyboard
            if ~strcmpi(MseKey, 'mouse')
                Screen('DrawTexture', winS, LT, [], LRectS, 270);
                Screen('DrawTexture', winS, RT, [], RRectS, 270);
                Screen('DrawTexture', winS, ET, [], ERectS, 270);
            end
            
            
        else
            %in split screen or something else
            % Screen('SelectStereoDrawBuffer', window, 1);
            
            Screen('FillRect', window, MGrey, centeredRect);
            Screen('DrawTexture', window, LFBTexture, [], LTextC, 270);
            Screen('DrawTexture', window, LSBTexture, [], RTextC, 270);
            Screen('DrawTexture', window, QTexture, [], QTextC, 270);
            %if there's the keyboard
            if ~strcmpi(MseKey, 'mouse')
                Screen('DrawTexture', window, LT, [], LRect, 270);
                Screen('DrawTexture', window, RT, [], RRect, 270);
                Screen('DrawTexture', window, ET, [], ERect, 270);
            end
            
            
        end
        
        
        %if you are in upright position
    else
        % Select left-eye image buffer for drawing (buffer = 0)
        Screen('SelectStereoDrawBuffer', window, 0);
        
        %position and draw left side text
        Screen('FillRect', window, MGrey, centeredRect);
        Screen('DrawTexture', window, LFBTexture, [], LTextC, 0);
        Screen('DrawTexture', window, LSBTexture, [], RTextC, 0);
        Screen('DrawTexture', window, QTexture, [], QTextC, 0);
        %if there's the keyboard
        if ~strcmpi(MseKey, 'mouse')
            Screen('DrawTexture', window, LT, [], LRect, 0);
            Screen('DrawTexture', window, RT, [], RRect, 0);
            Screen('DrawTexture', window, ET, [], ERect, 0);
        end
        
        
        % Select right-eye image buffer for drawing (buffer = 1)
        Screen('SelectStereoDrawBuffer', window, 1);
        if stereomode==10 && exist('slavescreen')
            % in dual display
            Screen('SelectStereoDrawBuffer', winS, 1);
            
            Screen('FillRect', winS, SGrey, centeredRectS);
            Screen('DrawTexture', winS, LFBTS, [], LTextS, 0);
            Screen('DrawTexture', winS, LSBTS, [], RTextS, 0);
            Screen('DrawTexture', winS, QTS, [], QTextS, 0);
            %if there's the keyboard
            if ~strcmpi(MseKey, 'mouse')
                Screen('DrawTexture', winS, LT, [], LRectS, 0);
                Screen('DrawTexture', winS, RT, [], RRectS, 0);
                Screen('DrawTexture', winS, ET, [], ERectS, 0);
            end
            
        else
            %in split screen or something else
            Screen('SelectStereoDrawBuffer', window, 1);
            
            Screen('FillRect', window, MGrey, centeredRect);
            Screen('DrawTexture', window, LFBTexture, [], LTextC, 0);
            Screen('DrawTexture', window, LSBTexture, [], RTextC, 0);
            Screen('DrawTexture', window, QTexture, [], QTextC, 0);
            %if there's the keyboard
            if ~strcmpi(MseKey, 'mouse')
                Screen('DrawTexture', window, LT, [], LRect, 0);
                Screen('DrawTexture', window, RT, [], RRect, 0);
                Screen('DrawTexture', window, ET, [], ERect, 0);
            end
            
        end
        
    end
    

    %present response screen
    RespOnset= Screen('Flip', window);
    
    if stereomode==10 && exist('slavescreen')
        ROSl= Screen('Flip', winS);
    end

    
    %if in mouse mode
    if strcmpi(MseKey, 'mouse')==1
        % set the initial position of the mouse to be in the centre of the screen
        SetMouse(xCenter, yCenter, window);
        ShowCursor();
        
            
    %get timestamp
    onsetTime = GetSecs;
        
        Click=[];
        Resp=[];
        while numel(Resp)<=0
            
            [Click, x, y, ~]= GetClicks(window,0);
            
            if stereomode==10 && exist('slavescreen')
                [Click, x, y, ~]= GetClicks(winS,0);
            end
            
            %%%%%% MAKESHIFT FOR DISPLAYS WITH WEIRD INDEXING %%%%%%%%%%
            if strcmpi(AORot, 'yes')
                y=y+LFSz(3);
            else
                y = y+LFSz(4);
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
            if numel(Click)>0
                
                %if the RDS was reported as top far
                if IsInRect(x,y, LCur) || ...
                        IsInRect(x,y,[LCur(1)+windowRect(3), LCur(2), LCur(3)+windowRect(3), LCur(4)])
                    
                    Resp=1;
                    offsetTime= GetSecs;
                    
                    %if the RDS was reported as top near
                elseif IsInRect(x,y,RCur) || ...
                        IsInRect(x,y,[RCur(1)+windowRect(3), RCur(2), RCur(3)+windowRect(3), RCur(4)])
                    
                    Resp=2;
                    offsetTime= GetSecs;
                    
                    %if the quit button was pessed
                elseif IsInRect(x,y, QCur) || ...
                        IsInRect(x,y,[QCur(1)+windowRect(3), QCur(2), QCur(3)+windowRect(3), QCur])
                    
                    Resp=0;
                    offsetTime= GetSecs;
                    Priority(0);
                    break
                end
            end
            
            if Resp==0
                Priority(0);
                break
            end
            
        end
        
        %if the keyboard is used instead
    else
        
        % Now we wait for a keyboard button signaling the observers response.
        % The left arrow key signals a "left" response and the right arrow key
        % a "right" response. You can also press escape if you want to exit the
        % program
        while isempty(Resp)
            %check for key presses
            [keyIsDown,secs, keyCode] = KbCheck;
            
            %if the escape key was chosen
            if keyCode(escapeKey)
                Resp=0;
                offsetTime= GetSecs;
                ShowCursor;
                sca;
                break
                
                %if descending was chosen
            elseif keyCode(leftKey)
                Resp = 1;
                offsetTime= GetSecs;
                
                %if ascending was chosen
            elseif keyCode(rightKey)
                Resp = 2;
                offsetTime= GetSecs;
            end
        end
        
    end
    
    
   end
    
ShowCursor
      %% ---- RECORD RESPONSES ----------------------------------------------
    %record number of correct responses
    
    %if the stimulus was far
    if (Resp==2 && (TrPlPr(t,1)-TrPlPr(t,2))<0) %and response was top near
        CorResp=[CorResp; 1];
        %if the test stimulus was near and response was top far
    elseif(Resp==1 && (TrPlPr(t,1)-TrPlPr(t,2))>0)
        CorResp=[CorResp; 1];
    else
        CorResp=[CorResp; 0];
    end
    
    if CorResp(end)==1
        % Fill the audio playback buffer with the audio data
        PsychPortAudio('FillBuffer', pahandle, FBeep);
        
        %play beep
        PsychPortAudio('Start', pahandle, 1, startCue, waitForDeviceStart);
        
    end
    
    
    
    %collate responses if beyond practice trials
    if t>nPrac
        %(:,1) Top disparity
        %(:,2) Bottom disparity
        %(:,3) response 2: top near; 1: top far
        %(:,4) correct=1, wrong =0
        %(:,5) reaction time
        RespList(t-nPrac,3)=Resp;
        RespList(t-nPrac, 4)=CorResp(t);
        RespList(t-nPrac,5)=offsetTime - onsetTime;
    end
    
    
    if Resp==0
        Priority(0);
        break
    end
    
    
    
end

%% ---- PRINT DATA OUT INTO LOG FILE --------------------------------------
%open the file to write into
file=fopen(filename,'wt');
fprintf(file,'%s	%s\n',date,filename);
fprintf(file,'file=''%s''\n',m);
fprintf(file,'observer=''%s''\n',observer);
fprintf('Saving results in "%s"\n',filename);
fprintf(file, '%s\n', 'Vernier1');

fprintf(file, '%s\t%d\t%d\n', 'Top Size: ', TopSz');
fprintf(file, '%s\t%d\t%d\n', 'Bot Size: ', BotSz');

fprintf(file, '%s\t%d\t%d\t%d\n', 'Background lum: ', BkgLum');
fprintf(file, '%s\t%d\t%d\t%d\n', 'Top lum: ', TopLum');
fprintf(file, '%s\t%d\t%d\t%d\n', 'Bot lum: ', BotLum');
fprintf(file, '%s\t%d\n', 'Test Presentation time: ', TsStimTime);

fprintf(file, '%s\t%d\n', 'nTrial Completed: ', t-nPrac);

%total number of non zero disp trails
nNZ=sum(RespList(:,1)-RespList(:,2)~=0);
fprintf(file, '%s%d%s%d\n\n', 'nTrials Correct = ', sum(RespList(:,4)), '/', nNZ);

fprintf(file, '%s\n', 'Response Log');
fprintf(file, '%s\t%s\t%s\t%s\t%s\t\n', ' Top Disp', 'Bot Disp',...
    'Response', 'Correct?', 'Reac Time');
fprintf(file, '%d\t%d\t%d\t%d\t%d\t\n', RespList');

sca
ShowCursor

disp(['nTrials Correct = ', num2str(sum(RespList(:,4))), '/', num2str(nNZ)])


%% ---- ANALYSIS ----------------------------------------------------------
% VernierXAnal_AO;