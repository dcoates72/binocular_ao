%% Copy/Hard-code variables when calling from Color_Hyperacuity

% Set variables Show_Nonius needs
% TODO: Some of these should be from the CLUT

% Screen 1:
MGrey=[0.1 0.1 0.1]; % background color: make it match our expt
centeredRect=screen_square_window;
foreground=[0.5 0 0]; % foreground. Make it positive contrast. not too bright.

windowRect=window_rect;
xCenter=X_center; yCenter=Y_center;

% Screen 2:
slavescreen=screenID2;
stereomode=10;
windowRectS=window_rect2;
winS=window2;

% For now, just copy many parameters from the first screen
SGrey=MGrey;
centeredRectS=centeredRect;
foreground2=[0.0 0.5 0];
xCenterS=xCenter; yCenterS=yCenter;


%% ---- FIXATION VARIABLES --------------------------------------------------

% These come from V4AO.m
PxRes = 2.32167e-3;

% size of nonius lines in deg visual angle 
%[horizontal arm length, vertical arm length, thickness of the lines]
Fix=[0.25, 0.25, PxRes*1*7]; % DRC: was 2*7=14 wide

%is the fixation dot on or off during the stimulus presentation?
FOnOff='off';

%how long to wait before allowing the trial to come on (ms)
FixDelay=100;

% fixation dot size specified in deg visual angle
%visual acuity ~1arcmin (or 0.0167degs at 6/6 vision)
%choose something that gives approx whole pixels
 DotSz=(4.64096e-3).*15; %AO

MseKey='keyboard';
KbName('UnifyKeyNames');
downKey= KbName('DownArrow');
     
%% ---- FIXATION STUFF ----------------------------------------------------
% fixation dot size in pixels
DPx= DotSz./PxRes;

%size of the fixation cross in px
FixCPx=Fix./PxRes;

%spacing between the fixation dot and the nonius lines in px
%[horizontal space, vertical space]
FixSp= 0.75.*FixCPx(1:2);

%coordinates of the nonius lines relative to the center of the screen
%[x-start, x-end; y-start, y-end]
%upper one will be presented in the L and lower on in R
%edges  are separated frpm the fix pt with a space equaling the dimensions of the fix pt
NonVT=[0, 0;-FixSp(2)-FixCPx(2), -FixSp(2)]; %top vertical
NonVB=[0, 0; FixSp(2), FixSp(2)+FixCPx(2)]; %bottom vertical
NonHL=[-FixSp(1)-FixCPx(1), -FixSp(1); 0, 0]; %left horizontal
NonHR=[FixSp(1), FixSp(1)+FixCPx(1); 0, 0]; %right horizontal

%coordinate of of the diagonal fixation alignment lines
TLD=[-FixSp(1)-FixCPx(1), -FixSp(1); ...
    -FixSp(2)-FixCPx(2), -FixSp(2)]; %top left diagonal
BLD=[-FixSp(1)-FixCPx(1), -FixSp(1); ...
    FixSp(2)+FixCPx(2), FixSp(2)]; %bottom left diagonal
TRD=[FixSp(1), FixSp(1)+FixCPx(1);...
    -FixSp(2), -FixSp(2)-FixCPx(2)];  %top right diagonal
BRD=[FixSp(1), FixSp(1)+FixCPx(1);...
    FixSp(2), FixSp(2)+FixCPx(2)]; %bottom right diagonal

%% ---- PRESENT FIXATION ----------------------------------------------
    F=0; FBut=false;
    while ~any(FBut)
        %count frames
        F=F+1;
        
        % Select left-eye image buffer for drawing (buffer = 0)
        Screen('SelectStereoDrawBuffer', window, 0);
        
        %draw background and dots, and a 6px frame around in the screen
        Screen('FillRect', window, MGrey, centeredRect);
        Screen('FrameRect', window, foreground, windowRect, 4);
        
        %draw nonius lines
        Screen('DrawLines', window, NonVT, FixCPx(3), foreground, [xCenter yCenter], 2);
        Screen('DrawLines', window, NonHL,  FixCPx(3), foreground, [xCenter yCenter], 2);
        Screen('DrawLines', window, TLD,  FixCPx(3), foreground, [xCenter yCenter], 2);
        Screen('DrawLines', window, BLD,  FixCPx(3), foreground, [xCenter yCenter], 2);
        Screen('DrawLines', window, TRD,  FixCPx(3), foreground, [xCenter yCenter], 2);
        Screen('DrawLines', window, BRD,  FixCPx(3), foreground, [xCenter yCenter], 2);
        
        %draw fixation dot
        Screen('DrawDots', window, [xCenter yCenter], DPx,foreground, [], 2);
        
        
        
        %if you are working with dual displays
        if (exist('slavescreen') && stereomode==10)
            % Select right-eye image buffer for drawing (buffer = 1)
            % Screen('SelectStereoDrawBuffer', winS, 1);
            
            Screen('FillRect', winS, SGrey, centeredRectS);
            Screen('FrameRect', winS, foreground2, windowRectS, 4);
            
            %draw nonius lines
            Screen('DrawLines', winS, NonVB, FixCPx(3), foreground2, [xCenterS yCenterS], 2);
            Screen('DrawLines', winS, NonHR,  FixCPx(3), foreground2, [xCenterS yCenterS], 2);
            Screen('DrawLines', winS, TLD,  FixCPx(3), foreground2, [xCenterS yCenterS], 2);
            Screen('DrawLines', winS, BLD,  FixCPx(3), foreground2, [xCenterS yCenterS], 2);
            Screen('DrawLines', winS, TRD,  FixCPx(3), foreground2, [xCenterS yCenterS], 2);
            Screen('DrawLines', winS, BRD,  FixCPx(3), foreground2, [xCenterS yCenterS], 2);
            
            %draw fixation dot
            Screen('DrawDots', winS, [xCenterS yCenterS], DPx,foreground2, [], 2);
            
        else
            % Select right-eye image buffer for drawing (buffer = 1)
            % Screen('SelectStereoDrawBuffer', window, 1);
            
            %draw fixation dot in the screen
            Screen('FillRect', window, MGrey, centeredRect);
            Screen('FrameRect', window, foreground, windowRect, 4);
            
            %draw nonius lines
            Screen('DrawLines', window, NonVB, FixCPx(3), foreground, [xCenter yCenter], 2);
            Screen('DrawLines', window, NonHR, FixCPx(3), foreground, [xCenter yCenter], 2);
            Screen('DrawLines', window, TLD,  FixCPx(3), foreground, [xCenter yCenter], 2);
            Screen('DrawLines', window, BLD,  FixCPx(3), foreground, [xCenter yCenter], 2);
            Screen('DrawLines', window, TRD,  FixCPx(3), foreground, [xCenter yCenter], 2);
            Screen('DrawLines', window, BRD,  FixCPx(3), foreground, [xCenter yCenter], 2);
            
            %draw fixation dot
            Screen('DrawDots', window, [xCenter yCenter], DPx,foreground, [], 2);
            
        end
        
        % Flip to the screen. This command basically draws all of our previous
        % commands onto the screen.
        if F==1
            Fvbl=[]; Fvbl=Screen('Flip', window);
            
            if exist('slavescreen') && stereomode==10
                Slvbl=[]; Slvbl=Screen('Flip', winS);
            end
            
            HideCursor();
        else
            Fvbl= Screen('Flip', window ); %, Fvbl+ ifi); % Not sure why it was doing this
            
            if exist('slavescreen') && stereomode==10
                Slvbl=Screen('Flip', winS ); %, Slvbl+ifi); % Ibid. Just a regular flip now.
            end
            
        end
        
        if F>6 % Show a minimum of 6 frames, but then advance with key/mouse
            if strcmpi(MseKey, 'mouse')==1
                [~, ~, FBut]= GetMouse;
            else
                %check for key presses
                [keyIsDown,secs, keyCode] = KbCheck;
                if keyCode(downKey)
                    FBut=1;
                end
            end
        end
        
    end

