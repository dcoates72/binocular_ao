% PARAMETER FILE FOR VERNIER STEREOACUITY
%presents two lines one on top of the other
%subject says if the top line is farther or nearer than the bottom line
%has a special parameter that asks if we wanna rotate the screens by 90deg
%if in dual monitor mode, opens up seperate windows, not usign stereomode
%if using keyboard, starts timing response time from onset of stimulus

clear all; close all; clc;

%% ----MONITOR VARIABLES AND ANNOYING ADMIN -------------------------------
% % %  distance of the screen from observer
% % in mm
% % 1044C: ~940mm
% % 1044D: ~1120mm
% Dist=472;
% 
% % dimensions of the left and right screen halves
% %how big does each eye recieve?
% % in mm [width, height]
% %1044D: [382.49, 305.99];
% %1044C: [184.15, 273.05]
% %Macbook13.3":[71.6123, 89.5154];
% ScnWH= [365./2, 274]; 

%pixel resolution (deg vis angle of 1 px)
%assume square px
%either specify distance and monitor variables, or pixel resolution
PxRes = 2.32167e-3;%f=180mm, Sharp xr10x.... %518 pixels for f=400mm, smallest letter roughly 20/2,

%stereomode
%stereomode=0: monocular
%stereomode=10: 2 monitors- display Limg on L monitor; Rimg on R monitor
%stereomode=4: 1 monitor split into 2 halves: L on L half; R on R half
stereomode=10;

%is the display rotated by 90deg clockwise?
AORot='no';
%is the display reflected L-R?
%if so, the program unflips the stimuli that was calculated from the AORot
%output
AOFlip='no';

%wanna use CLUT?{[:,1: which screen], [:,2: which clut file]}
%if on laptop, or other debugging device leave blank to not use any clut files
%otherwise, specify which screen to draw to, and the clut file
%0: master screen; 1: slave screen
%specify only the monitor you wanna draw to in stereomode 4
%otheriwse specify both [0] and [1] and the respective cluts in stereomode 10
UseLUT={
   % DRC
   % [1],
   % ['/Users/bhoagland/Documents/Cher/ProjCalibration/CalibFiles/LTI1Lum.mat'];...
   % [2], ['/Users/bhoagland/Documents/Cher/ProjCalibration/CalibFiles/RTI1Lum.mat'];...
    };

%check the display with screen test?
%if no, PTB will apply Screen('Preference', 'SkipSyncTests', 2);
ScrnTst='no'; % DRC

%are you responding with the mouse or the keyboard?
%mouse: 'mouse'; keyboard: 'keyboard'
%will be set to keyboard by default
MseKey='keyboard';

%where to save the data?
%SavePath=['/Users/bhoagland/Documents/Cher/VernierStereo/results/'];
SavePath=['../results']; % DRC

%% ---- RESPONSE TEXT VARIABLES -------------------------------------------
%text font size
TxtSz=60;

%only applicable if using the kryboard:
%icon sizes [w, h]
IcSz=[120, 120];

ResponseUseImages='no';

%% ---- FIXATION VARIABLES --------------------------------------------------

% size of nonius lines in deg visual angle 
%[horizontal arm length, vertical arm length, thickness of the lines]
Fix=[0.25, 0.25, PxRes*1*7]; % DRC: was 2*7=14 wide

%is the fixation dot on or off during the stimulus presentation?
FOnOff='off';

%how long to wait before allowing the trial to come on (ms)
FixDelay=100;

% fixation dot size specified in deg visual angle
%visual acuity ~1arcmin (or 0.0167degs at 6/6 vision)
%choose something that gives approx whole pixels
 DotSz=(4.64096e-3).*15; %AO


%% ---- TRIAL VARIABLES ---------------------------------------------------

%luminance of the background (0-1)
BkgLum=0.5;

%luminance of the top line (0-1; 0:darkest; 1:lightest)
TopLum=0;

%luminance of the bottom line
BotLum=0;

%size of the top line in deg visual angle
%[arm length, thickness of the lines]
TopSz=[0.5, PxRes*1.*7]; % DRC: was PxRes*2*7 (14 pixels thick)

%size of the bottom line in deg visual angle
%[arm length, thickness of the lines]
BotSz=[0.5, PxRes*1.*7]; % DRC ibid.

%spacing  between the lines in deg visual angle
LinSpc= 0.2;

%top line disparity levels in degs
%in comparison to the fixation
TopDisp=[-0.01, -0.0075, -0.0050, -0.0025, 0, 0.0025, 0.0050, 0.0075, 0.01];

%bottom line disparity levels in degs
%in comparison to the fixation
%if more than one value is specified, every disparity of the top line would
%be compared with every value of the bottom line
BotDisp=[0];

%orientation of the lines
%the line are always 180eg to each other
%0deg means horizontal where the top line becomes the right one
%90deg means vertical.
%i.e., the lines rotate anticlocwise where the right line becomes vertical
Ori=90;

%center position (deg visual angle) of the stimulus
%[0,0] = [xCenter, yCenter]
%[-ve, -ve]=top left quadrant
%[+ve, +ve]= bottom right quadrant
StimPos=[0, 0];

%number of practice trials (data not counted)
nPrac=5;

%how many reps?
nT=5;

%stimulus presentation times
TsStimTime=500;

%how many frames to wait before updating the image?
waitframes=1;


%% ---- SOUND -------------------------------------------------------------
% Number of channels and sampling Frequency of the sound
nrchannels = 1;
Samp = 48000;

%frequency of the sound
Freq=500;

% Length of the beep in ms 
FBL = 75;

%Volume (between 0-1)
Vol=0.5;


%% run the program
m=mfilename;
Vernier4;
