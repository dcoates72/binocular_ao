clear all
close all
clear mex
warning off

%% High-level control parameters
doTCA = 0; % 0 (no) or 1 (yes) to consider TCA

%% TCA red against green

if doTCA == 1
    wait4response = 1;
    while wait4response == 1
      whetherMeasureTCAinput = input('Measure TCA (Y) or Input TCA (N)?', 's');
      if strcmp(whetherMeasureTCAinput, 'Y') || strcmp(whetherMeasureTCAinput, 'y')
        whetherMeasureTCA = 1;
        wait4response = 0;
      elseif strcmp(whetherMeasureTCAinput, 'N') || strcmp(whetherMeasureTCAinput, 'n')
        whetherMeasureTCA = 0;
        wait4response = 0;
      end
    end

    if whetherMeasureTCA == 1
      [xTCA, yTCA] = MeasureTCA();
    elseif whetherMeasureTCA == 0
      xTCA = input('TCA in X direction (red against green):');
      yTCA = input('TCA in Y direction (red against green):');
    end
    TCA = [xTCA yTCA];

    save('TCA')

    clear all
    clear mex
    clear all hidden

    load('TCA')
    
else
    TCA = [0 0];
end % doTCA

%% High-level control parameters 2

Apparatus='UH2'; % 'UW', 'UH1', 'UH2' or 'UR'.. See below for descriptions of each

%% Info
SubjectInitials = 'xxx'; %input('Enter Subject Initials:', 's'); % save time during testing
RoundNum = 1; %input('Enter Round Number:'); 

%% Color setup
if strcmpi(Apparatus, 'UW')
    load('gamma_data.mat')
    load('LuminousEfficiencyFunction.mat')
    inverted_gamma_params = GammaCorrection(gamma_data);
    % RGLuminanceScaling = ScaleRGIntensity(Wavelength, LuminousEfficiencyFunction, R0Intensity, G0Intensity, R255Intensity, G255Intensity);
    RGLuminanceScaling = 1;

    BackgroundCol = [0.1, 0.1, 0.1];
    correctedBackgroundColBit = CorrectGammaBitRG(BackgroundCol, inverted_gamma_params, RGLuminanceScaling);
else
    % TODO/temporary: for now, hardcode background luminance of 0.1 (no gamma)
    correctedBackgroundColBit=0.1;
end

background_level_makelines=0.1;
% This is used inside makelines() for non-target pixels in each plane.. Not
% sure if this is (or should be) same as correctedBackgroundColBit
%% Setup Psychtoolbox display

Screen('Preference', 'SkipSyncTests', 1);

if strcmpi(Apparatus, 'UW')
    screenID = max(Screen('Screens'));
    % [window, window_rect] = PsychImaging('OpenWindow', screenID, correctedBackgroundColBit, [0 0 640 480]);
    [window, window_rect] = PsychImaging('OpenWdrcindow', screenID, [0 0 0]);
elseif strcmpi(Apparatus, 'UH1')
    PsychDefaultSetup(2); % Copied from Rochester. Seems necessary, but not sure why.
    InitializeMatlabOpenGL; % Not sure if necessary--copied from Rochester code
    screenID = max(Screen('Screens'));
    [window, window_rect] = PsychImaging('OpenWindow', screenID, [0 0 0]);
    stereo=0;
elseif strcmpi(Apparatus, 'UH2')
    PsychDefaultSetup(2); % Copied from Rochester. Seems necessary, but not sure why.
    screenID = max(Screen('Screens')); 
    screenID2 = screenID-1;
    [window, window_rect] = PsychImaging('OpenWindow', screenID, [0 0 0]);    
    [window2, window_rect2] = PsychImaging('OpenWindow', screenID2, [0 0 0]);
    stereo=1;
end % Which Screen setup for system

Screen('BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
if stereo
    Screen('BlendFunction', window2, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
end
    
% Assume screens have same dimensions, compute params from main screen
[X_pixels, Y_pixels] = Screen('WindowSize', window);
available_win_size = min(X_pixels, Y_pixels);
[X_center, Y_center] = RectCenter(window_rect);
screen_square_window=[X_center - round(available_win_size / 2), Y_center - round(available_win_size / 2), X_center + round(available_win_size / 2), Y_center + round(available_win_size / 2)];

% Clear each screen to blank square
Screen('FillRect', window, correctedBackgroundColBit, screen_square_window);
Screen('Flip', window);
if stereo
    Screen('FillRect', window, correctedBackgroundColBit, screen_square_window);
    Screen('Flip', window);
end

%% Stimulus parameters

%StimulusDuration = 0.1;
Duration_Flips = 6; % 6 flips @ 60hz = 100 ms

StimulusRot90=1; % Whether to rotate the stimulus 90 degrees CCW (0=vertical lines, 1=stacked horizontal lines)

if strcmpi(Apparatus, 'UW')
    % Seattle system has pixels that are 800 pix/deg (13.3pix/min)
    line_length = 200;
    line_width  = 4;
    image_size  = 800; %1 deg
elseif strcmpi(Apparatus, 'UH1')
    % Houston system has pixels that are 0.0028 deg (360 pix/deg=6pix/min)
    % Two tiny (5") bright 1920x1080 LCD screens at distance
    % This Apparatus shows both (R/G) planes on a single display.
    line_length = 100;
    line_width  = 2;
    image_size  = 800;
elseif strcmpi(Apparatus, 'UH2')
    % Houston system has pixels that are 0.0028 deg (360 pix/deg=6pix/min)
    % Two tiny (5") bright 1920x1080 LCD screens at distance.
    line_length = 100;
    line_width  = 2;
    image_size  = 800; 
elseif Apparatus=='UR'
    % Rochester system has pixels that are 2.32167e-3 deg (430.7244 pix/deg)
    line_length = 100;
    line_width  = 2;
    image_size  = 800;
end    

%% Sounds

soundFs = 48000;
% feedback.correct = audioread('correct.mp3', 'native');
% feedback.wrong = audioread('wrong.mp3', 'native');
% feedback.signal = audioread('signal.mp3', 'native');

% Make a beep which we will play back to the user
feedback.signal = MakeBeep(500, 0.075, soundFs);

%% Keyboard response coede
% 
% key_code.redgreen = 122; % Z
% key_code.blackwhite = 120; % X
key_code.abort = 27; % esc
key_code.start = 32; % space

UP = 30;
DOWN = 31;
LEFT = 28;
RIGHT = 29;

key_code.redleft  = LEFT; % left arrow
key_code.redright = RIGHT; % right arrow

key_code.oneyellow  = UP; % up arrow
key_code.twoyellow = DOWN; % down arrow

%%%% For Horizontal lines %%%%%%%%%%%%%%
% Did not change the key codes. 
% Will use "up" for "red-up" 
% Will use "down" for "red-down"
% Will use "right" for "two-lines yellow"
% Will use "left" for "one-line yellow"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

key_code.first = 49; % 1
key_code.second = 50; % 2

%% Trials and spacings

num_trials_per_spacing = 10;
spacings               = [0,8,12,16,20,30,40];
stim_codes             = [1 2 3 4]; %1:red left, 2:red right, 3:two yellow, 4:one yellow
allcombo               = combvec(stim_codes,spacings)';
allcombo_trials        = repmat(allcombo, [num_trials_per_spacing,1]);
sequence_rand          = allcombo_trials(randperm(size(allcombo_trials,1)),:); %[stimcode spacing]

responses              = zeros(size(sequence_rand,1),5); %[num_trial,stimcode,spacing,stimresponse,correct/incorrect]

%% Experiment

for ii = 1 : size(sequence_rand,1)

  % Break
% 
%   need2getkey = 1;
%   while need2getkey
%     disp('Taking a break. Press SPACE to start:')
%       response_key = getkey();
%       if response_key == key_code.start
%         need2getkey = 0;
%       end
%   end

    disp(['Stimulus code/spacing(px): ' num2str(sequence_rand(ii,1)) '/' num2str(sequence_rand(ii,1))])

    stim = makelines (image_size, TCA, sequence_rand(ii,1), sequence_rand(ii,2),line_width,line_length,background_level_makelines);
    
    if StimulusRot90
        stim_plane_red = rot90(stim.R,-1); % Rotate 90 degrees counter-clockwise
        stim_plane_green = rot90(stim.G,-1);
    else
        stim_plane_red = stim.R;
        stim_plane_green = stim.G;        
    end
    
    if stereo
        stim = repmat(zeros(size(stim_plane_red)), [1 1 3]);
        stim2 = repmat(zeros(size(stim_plane_red)), [1 1 3]);
        stim(:,:,1) = stim_plane_red;
        stim2(:,:,2) = stim_plane_green;

        % Do gamma correction of each plane here?
        %if strcmpi(Apparatus, 'UW')
        %    stim = CorrectGammaBitTXTRG(stim, inverted_gamma_params, RGLuminanceScaling);
        %end

        stim_tex = Screen('MakeTexture', window, stim);
        stim_tex2 = Screen('MakeTexture', window2, stim2);

    else
        stim = repmat(zeros(size(stim_plane_red)), [1 1 3]);
        stim(:,:,1) = stim_plane_red;
        stim(:,:,2) = stim_plane_green;

        if strcmpi(Apparatus, 'UW')
            stim = CorrectGammaBitTXTRG(stim, inverted_gamma_params, RGLuminanceScaling);
        end

        stim_tex = Screen('MakeTexture', window, stim);
    end

    Screen('FillRect', window, correctedBackgroundColBit, screen_square_window );
    Screen('Flip', window);
    if stereo

        Show_Nonius
        
        %Screen('FillRect', window2, correctedBackgroundColBit, screen_square_window );
        %Screen('Flip', window2);
    else
        Screen('FillRect', window, correctedBackgroundColBit, screen_square_window );
        Screen('Flip', window);
    end

    sound(feedback.signal, soundFs)
    pause(0.5)
    
    for f=1:Duration_Flips
        Screen('FillRect', window, correctedBackgroundColBit, screen_square_window);
        Screen('DrawTexture', window, stim_tex);
        if stereo
            Screen('FillRect', window2, correctedBackgroundColBit, screen_square_window );
            Screen('DrawTexture', window2, stim_tex2);
            Screen('Flip', window2);
        end   
        Screen('Flip', window);
    end
    %pause(StimulusDuration)

    Screen('FillRect', window, correctedBackgroundColBit, screen_square_window);
    Screen('Flip', window);
    if stereo
        Screen('FillRect', window2, correctedBackgroundColBit, screen_square_window );
        Screen('Flip', window2);
    end

    need2getkey = 1;
    
    while need2getkey
        response_key = getkey;
        if response_key == key_code.redleft
            Response = 1;
            need2getkey = 0;
        elseif response_key == key_code.redright
            Response = 2;
            need2getkey = 0;
        elseif response_key == key_code.twoyellow
            Response = 3;
            need2getkey = 0;   
        elseif response_key == key_code.oneyellow
            Response = 4;
            need2getkey = 0;
        elseif response_key == key_code.abort
            mkdir('ColorHyperAcuity_DATA', SubjectInitials)
            dir = ['./ColorHyperAcuity_DATA/' SubjectInitials '/'];
            filename = [dir SubjectInitials '_trial#' num2str(RoundNum) '_' datestr(datetime,'yy_mm_dd_HH_MM_SS') '.mat'];
            save(filename, 'responses')
            sca
            return
        else
            need2getkey = 1;
      end
    end
    
    if (sequence_rand(ii,1)==Response), correct = 1; else  correct =0; end;     
        responses(ii,:) = [ii,sequence_rand(ii,1), sequence_rand(ii,2),Response,correct];
     %response = [num_trial,stimcode,spacing,stimresponse,correct/incorrect]

    disp(['Response: ' num2str(Response)])

  end



mkdir('ColorHyperAcuity_DATA', SubjectInitials)
dir = ['./ColorHyperAcuity_DATA/' SubjectInitials '/'];
filename = [dir SubjectInitials '_trial#' num2str(RoundNum) '_' datestr(datetime,'yy_mm_dd_HH_MM_SS') '.mat'];
save(filename, 'responses')

sca
