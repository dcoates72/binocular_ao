
num_trials_per_spacing = 10;
spacings               = [0,8,12,16,20,30,40];
stim_codes             = [1 2 3]; %  4]; %1:red left, 2:red right, 3:two yellow, 4:one yellow
allcombo               = combvec(stim_codes,spacings)';

allcombo(allcombo(:,1)==3 & allcombo(:,2)==0,:) = [4,0]; % replace the 3,0 condition with 4,0
allcombo=[allcombo; [4,0]]; % add another 4,0 condition, so we'll have 2*10 trials for that

allcombo_trials        = repmat(allcombo, [num_trials_per_spacing,1]);
sequence_rand          = allcombo_trials(randperm(size(allcombo_trials,1)),:); %[stimcode spacing] 


